#include "ros/ros.h"

#include <mutex>

#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>

#include <image_transport/image_transport.h>

#include <sensor_msgs/Image.h>
#include <geometry_msgs/PointStamped.h>

geometry_msgs::Point curr_pt;
std::mutex pt_mutex;

void image_cb(const sensor_msgs::ImageConstPtr& msg) {
	try {
		cv::Mat img = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
		pt_mutex.lock();
		if (curr_pt.x != 0 && curr_pt.y != 0) {
			cv::Mat overlay;
			double alpha = 0.6;
			img.copyTo(overlay);

			cv::circle(overlay, cv::Point(curr_pt.x * img.cols, curr_pt.y * img.rows), 32.0, cv::Scalar(125, 125, 50), -1);
			cv::circle(overlay, cv::Point(curr_pt.x * img.cols, curr_pt.y * img.rows), 5.0, cv::Scalar(0, 0, 0), -1);

			cv::addWeighted(overlay, alpha, img, 1 - alpha, 0, img);

		}
		pt_mutex.unlock();

		cv::imshow("Gaze Overlay", img);
		cv::waitKey(1);
	} catch (cv_bridge::Exception& e) {
		ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
	}
}

void gaze_cb(const geometry_msgs::PointStampedConstPtr& msg) {
	pt_mutex.lock();
	curr_pt.x = msg->point.x;
	curr_pt.y = msg->point.y;
	pt_mutex.unlock();
}

int main(int argc, char **argv) {
	
	ros::init(argc, argv, "gaze_overlay_node");
	ros::NodeHandle nh("~");

	image_transport::ImageTransport it(nh);

	std::string gaze_topic = "/gaze_point";
	std::string image_topic = "/screen_grab";

	ros::Subscriber gaze_sub = nh.subscribe(gaze_topic, 1, gaze_cb);
	image_transport::Subscriber screen_sub = it.subscribe(image_topic, 1, image_cb);

	ROS_INFO("Init done");
	ros::spin();

	return 0;
}
