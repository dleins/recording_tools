#include <opencv2/highgui/highgui.hpp>
#include <mutex>
#include <queue>

#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>

#include "ros/ros.h"

#include <image_transport/image_transport.h>
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/Point.h"
#include "sensor_msgs/Image.h"
#include "std_msgs/String.h"

struct WriteBufferObject
{ 
    cv::Mat screen, webcam;
    geometry_msgs::Point gaze;
    int frame_nr;

    WriteBufferObject()=default;
    WriteBufferObject(cv::Mat img1, cv::Mat img2, geometry_msgs::Point p, int frn): screen(img1), webcam(img2), gaze(p), frame_nr(frn) {} 

};

void screen_cb(const sensor_msgs::ImageConstPtr& msg);
void webcam_cb(const sensor_msgs::ImageConstPtr& msg);
void gaze_cb(const geometry_msgs::PointStampedConstPtr& msg);

ros::Timer timer_;

geometry_msgs::Point last_gaze_point_;
cv::Mat last_screen_image_;
cv::Mat last_webcam_image_;

std::mutex screen_mutex_;
std::mutex webcam_mutex_;
std::mutex gaze_mutex_;
std::mutex queue_mutex_;

std::queue<WriteBufferObject> write_buffer_;

int frame_number_ = 0;
double update_rate_;
bool triggered_shutdown_;

void screen_cb(const sensor_msgs::ImageConstPtr& msg) {
    screen_mutex_.lock();
    last_screen_image_ = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
    screen_mutex_.unlock();
}

void webcam_cb(const sensor_msgs::ImageConstPtr& msg) {
    webcam_mutex_.lock();
    last_webcam_image_ = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
    webcam_mutex_.unlock();
}

void gaze_cb(const geometry_msgs::PointStampedConstPtr& msg) {
    gaze_mutex_.lock();
    last_gaze_point_.x = msg->point.x;
    last_gaze_point_.y = msg->point.y;
    gaze_mutex_.unlock();
}

void loop_cb(const ros::TimerEvent& event) {
    if (not triggered_shutdown_ && ros::ok())
    {
        gaze_mutex_.lock();
        webcam_mutex_.lock();
        screen_mutex_.lock();

        if (not (last_webcam_image_.cols == 0 or last_screen_image_.cols == 0)) {
            queue_mutex_.lock();
            WriteBufferObject bo(last_screen_image_, last_webcam_image_, last_gaze_point_, frame_number_);
            write_buffer_.push(bo);
            queue_mutex_.unlock();
        }

        gaze_mutex_.unlock();
        webcam_mutex_.unlock();
        screen_mutex_.unlock();
        frame_number_++;
    }
}

void shutdown_cb(const std_msgs::String::ConstPtr& msg) {
    ROS_INFO("TERMINATING EXECUTION");
    triggered_shutdown_ = true;
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "recorder");

    triggered_shutdown_ = false;
    update_rate_ = 30;

    ros::NodeHandle nh;
    image_transport::ImageTransport it(nh);

    std::string screen_topic = "/screen_grab";
    std::string gaze_topic = "/gaze_point";
    std::string webcam_topic = "/usb_cam/image_raw";
    std::string shutdown_topic = "/recorder/shutdown";

    ROS_INFO("INITIALIZING SUBSCRIBERS!");

    image_transport::Subscriber screen_sub = it.subscribe(screen_topic, 1, screen_cb);
    image_transport::Subscriber webcam_sub = it.subscribe(webcam_topic, 1, webcam_cb);
    ros::Subscriber gaze_sub = nh.subscribe(gaze_topic, 1, gaze_cb);

    ros::Subscriber shutdown_sub = nh.subscribe(shutdown_topic, 1, shutdown_cb);
    
    ROS_INFO("STARTING TIMER");

    timer_ = nh.createTimer(ros::Duration(1.0/update_rate_), loop_cb);

    ROS_INFO("CREATING VIDEO WRITERS");

    cv::VideoWriter webcam_writer = cv::VideoWriter("webcam.mp4", CV_FOURCC('H', '2', '6', '4'), update_rate_, cv::Size(640, 480));
    cv::VideoWriter screen_writer = cv::VideoWriter("screen.mp4", CV_FOURCC('H', '2', '6', '4'), update_rate_, cv::Size(1600, 1024));

    ROS_INFO("ENTERING LOOP");

    WriteBufferObject currentWrite;
    while (triggered_shutdown_ == false)
    {
        while (not write_buffer_.empty())
        {
	    ROS_INFO_ONCE("WRITING FIRST FRAME!");
            queue_mutex_.lock();
            currentWrite = write_buffer_.front();
            write_buffer_.pop();
            queue_mutex_.unlock();

		    if (currentWrite.gaze.x != 0 && currentWrite.gaze.y != 0) {
                cv::Mat overlay;
                double alpha = 0.6;
                currentWrite.screen.copyTo(overlay);

                cv::circle(overlay, cv::Point(currentWrite.gaze.x * overlay.cols, currentWrite.gaze.y * overlay.rows), 32.0, cv::Scalar(125, 125, 50), -1);
                cv::circle(overlay, cv::Point(currentWrite.gaze.x * overlay.cols, currentWrite.gaze.y * overlay.rows), 5.0, cv::Scalar(0, 0, 0), -1);
                cv::addWeighted(overlay, alpha, currentWrite.screen, 1 - alpha, 0, currentWrite.screen);
            }

            cv::resize(currentWrite.webcam, currentWrite.webcam, cv::Size(640, 480), 0, 0, CV_INTER_LINEAR);
            cv::resize(currentWrite.screen, currentWrite.screen, cv::Size(1600, 1024), 0, 0, CV_INTER_LINEAR);

            webcam_writer.write(currentWrite.webcam);
            screen_writer.write(currentWrite.screen);
        }
        ros::spinOnce();
    }

    while (not write_buffer_.empty())
    {
        queue_mutex_.lock();
        currentWrite = write_buffer_.front();
        write_buffer_.pop();
        queue_mutex_.unlock();

        if (currentWrite.gaze.x != 0 && currentWrite.gaze.y != 0) {
            cv::Mat overlay;
            double alpha = 0.6;
            currentWrite.screen.copyTo(overlay);

            cv::circle(overlay, cv::Point(currentWrite.gaze.x * overlay.cols, currentWrite.gaze.y * overlay.rows), 32.0, cv::Scalar(125, 125, 50), -1);
            cv::circle(overlay, cv::Point(currentWrite.gaze.x * overlay.cols, currentWrite.gaze.y * overlay.rows), 5.0, cv::Scalar(0, 0, 0), -1);
            cv::addWeighted(overlay, alpha, currentWrite.screen, 1 - alpha, 0, currentWrite.screen);
        }

        cv::imshow("test", currentWrite.screen);
        cv::waitKey(1);

        webcam_writer.write(currentWrite.webcam);
        screen_writer.write(currentWrite.screen);
    }

    ROS_INFO("LEFT WHILE LOOP");
    webcam_writer.release();
    screen_writer.release();
    ROS_INFO("CLOSED WRITERS");

    return 0;
}
